This app will tell you about your facebook likes which may be situated nearby.

The pre requisites for running this app are
1. Internet connection
2. Gps enabled

How it works
Just download the apk.
Enable gps
Sign in to facebook
Click on bored button
Look for your likes nearby

The folder src has the source code and bin has the apk
In the src folder 
com/fb/fbNotifier folder has following files
MainActivity
This has most of the codes. It takes data of your facebook likes from the graph api of facebook
and sends it to google maps api to see the locations nearby
InterestList
This consists of the interests nearby
MyLocation
Again it is a class consisting of getters and setters used for encapsulating data.

