My app is based on these facebook likes. This app will basically retrieve the things you like from your facebook likes and tell you where can you find it. For example suppose you are hanging around somewhere and you need to know if there is some interesting place from your facebook likes, just  turn on this app and you can easily find places. For  example you have liked "Crossword" in your facebook likes and you are getting bored at Mangalore , It will suggest you about a nearby Crossword outlet, if there is within a few kilometers.
Steps
Step1
Now for my App I used facebook  sdk. The Facebook SDK for Android is the easiest way to integrate your Android app with Facebook's platform. The SDK provides support for Login with Facebook authentication, reading and writing to Facebook's APIs,  and many other things. Now I used this to retrieve the facebook Likes. It can be retrieved by either the graph or the facebook query language.
Step 2
The output was a json file which needed to be parsed to get the name of the likes.
Step 3:
The current location of the user was retrieved using gps.
Step 4:
Now this likes was queried to the Google geocoder. The  result was then checked and the distance between the current and the location retrieved was found out. Only places which lie at 10 kms approximately was retrieved a and was sent on the listview.
Feature:
�        Recieves facebook likes

�        Finds current location

�        Check in map

�        Displays nearby locations.
Open the apk and wait till your name displays and the current location will be seen on toast. Click on the button Bored and get the list of places nearby that interests you