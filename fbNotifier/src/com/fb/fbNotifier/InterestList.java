package com.fb.fbNotifier;


import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class InterestList extends Activity {
	private ArrayAdapter<String> listAdapter;
	private ListView listView1 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_list);
        Bundle b=this.getIntent().getExtras();
        
		ListView listView1 = (ListView) findViewById(R.id.listView1);
		String[] array=b.getStringArray("place");
		if (array.length == 0) {
			TextView display_error = (TextView) findViewById(R.id.display_error);
			display_error.setText("SORRY NO LIKES NEARBY");
        }
		ArrayList<String> placelist = new ArrayList<String>();  
		placelist.addAll( Arrays.asList(array) );  

		listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, placelist);
		for(int i=0;i<array.length;i++)
		{
			System.out.println(array[i]);
		}
		listView1.setAdapter( listAdapter ); 

        // Show the Up button in the action bar.
       
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_interest_list, menu);
        return true;
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
